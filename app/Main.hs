{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections       #-}

module Main where

import qualified Data.Char as Char
import           Configuration.Dotenv     (defaultConfig, loadFile)
import           Control.Concurrent.Async
import           Control.Monad
import qualified Network.URI              as URI
import           Pipes
import           Pipes.Concurrent

import qualified Args                     as Args
import qualified Cache
import           DB                       (DB, runDB)
import           Post                     (Post, postUrl)
import qualified Post
import qualified Reddit
import qualified Download


main :: IO ()
main = do
  _ <- loadFile defaultConfig
  Args.parse >>= mainWithArgs
  -- case result of
  --   Left err   -> putStrLn err
  --   Right args -> mainWithArgs args


mainWithArgs :: Args.Options -> IO ()
mainWithArgs args = do
  let jobs = zip (Args.subreddits args) (repeat 200)
  (out1, in2) <- spawn (bounded 50)
  (out2, in3) <- spawn (bounded 100)
  (out3, in4) <- spawn (bounded 100)

  cs <- replicateM 3 $ async $ runDB args $ do
          runEffect $ fromInput in4  >-> downloadWorker
          liftIO $ performGC
  bs <- replicateM 3 $ async $ runDB args $ do
          runEffect $ fromInput in3  >-> postFilter args >-> toOutput out3
          liftIO $ performGC
  as <- replicateM 3 $ async $ do
          runEffect $ fromInput in2  >-> subredditFetcher >-> toOutput out2
          performGC
  a  <- async $ do runEffect $ each jobs >-> toOutput out1
                   performGC

  mapM_ wait (a:as ++ bs ++ cs)


data Action
  = Ignore
  | Fetch
  | Link Cache.CachedFile
  deriving (Show, Eq)

downloadWorker :: Consumer (Post, Action) DB ()
downloadWorker = do
  forever $ do
    (post, action) <- await

    let subdir = map Char.toLower (Post.postSubreddit post)
    let filename = destName post

    case action of
      Ignore -> return ()
      Fetch -> do
        case URI.parseURI (postUrl post) of
          Nothing -> return ()
          Just uri -> do
            result <- liftIO (Download.toTemp uri)
            lift $ Cache.notifyDownload post result >>= \case
              Nothing -> return ()
              Just cacheFile -> Cache.link cacheFile subdir filename
      Link cacheFile ->
        lift $ Cache.link cacheFile subdir filename

destName :: Post -> String
destName post =
  Post.postId post ++ " - " ++ postTitleForFilename post

postTitleForFilename :: Post -> String
postTitleForFilename = filter (\ch -> Char.isAlphaNum ch || ch == ' ') . Post.postTitle


postFilter :: Args.Options -> Pipe (Int, Post) (Post, Action) DB ()
postFilter args = forever $ do
  (rank, post) <- await
  result <- lift $ Cache.findPost rank post
  case result of
    Left err ->
        liftIO $ print err
    Right (postStatus, downloadStatus) -> do
        let action =
              case (Args.dupsOption args, postStatus, downloadStatus) of
                   (_, _, Cache.NotWanted) -> Ignore
                   (Args.OnlyNew, _, Cache.Remote)      -> Fetch
                   (Args.OnlyNew, _, _)                          -> Ignore
                   (Args.IncludeOld, _, Cache.Cached cachedFile) -> Link cachedFile
                   (Args.IncludeOld, _, Cache.Remote)   -> Fetch
        yield (post, action)

subredditFetcher :: Pipe (String, Int) (Int, Post) IO ()
subredditFetcher = forever $ do
  (name, limit) <- await
  result <- Reddit.fetchPosts name limit yield
  case result of
    Left err -> liftIO (print err)
    Right () -> return ()
