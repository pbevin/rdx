DROP TABLE IF EXISTS reddit_posts;

CREATE TABLE reddit_posts (
  subreddit VARCHAR(255) NOT NULL,
  reddit_id VARCHAR(255) NOT NULL,
  url VARCHAR(1024) NOT NULL,
  title TEXT NOT NULL,
  max_score INTEGER NOT NULL,
  min_pos INTEGER NOT NULL,
  is_new BOOLEAN NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT NOW(),

  PRIMARY KEY (subreddit, reddit_id)
);

create index post_subreddit on reddit_posts (subreddit);
create index post_url on reddit_posts (url);
create index post_created_at on reddit_posts (created_at);
