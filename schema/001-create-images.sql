DROP TABLE IF EXISTS images CASCADE;
DROP TYPE IF EXISTS imgtype;

CREATE TYPE imgtype AS ENUM ('jpg', 'gif', 'png', 'movie');

CREATE TABLE images (
  sha1 CHAR(64) NOT NULL PRIMARY KEY,
  basename VARCHAR(10) NOT NULL,
  imgtype imgtype NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT NOW()
);

CREATE INDEX img_type on images (imgtype);
CREATE UNIQUE INDEX img_basename on images (basename);
CREATE INDEX img_created_at ON images (created_at);
