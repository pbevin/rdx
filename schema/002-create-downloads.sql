DROP TABLE IF EXISTS downloads;

CREATE TABLE downloads (
  url VARCHAR(1024) NOT NULL PRIMARY KEY,
  error TEXT,
  sha1 CHAR(64) references images(sha1),
  created_at TIMESTAMP NOT NULL DEFAULT NOW()
);

create index dl_sha1 on downloads (sha1);
create index dl_created_at on downloads (created_at);
