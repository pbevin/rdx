{-# LANGUAGE ApplicativeDo #-}

module Args
  ( Options(..)
  , DuplicateOption(..)
  , parse
  ) where

import Options.Applicative

data Options = Options
  { dupsOption :: DuplicateOption
  , subreddits :: [String]
  , cacheDir :: FilePath
  , finalDir :: FilePath
  } deriving (Show, Eq)

data DuplicateOption
  = OnlyNew
  | IncludeOld
  deriving (Show, Eq)

parse :: IO Options
parse = execParser rdxParser

rdxParser :: ParserInfo Options
rdxParser =
  info
    (parser <**> helper)
    (fullDesc <> progDesc "Download files from Reddit")

parser :: Parser Options
parser = Options <$> parseDupsOption <*> parseSubreddits <*> parseCacheDir <*> parseFinalDir

parseDupsOption :: Parser DuplicateOption
parseDupsOption = parseOnlyNew <|> parseIncludeOld

parseOnlyNew :: Parser DuplicateOption
parseOnlyNew = flag' OnlyNew (long "only-new" <> help "Don't include files already in the cache")

parseIncludeOld :: Parser DuplicateOption
parseIncludeOld = flag' IncludeOld (long "include-old" <> help "Include all files in the subreddits")

parseSubreddits :: Parser [String]
parseSubreddits = some (argument str (metavar "SUBREDDITS..."))

parseCacheDir :: Parser FilePath
parseCacheDir = strOption (long "cache-dir" <> metavar "DIR" <> value "tmp/cache")

parseFinalDir :: Parser FilePath
parseFinalDir = strOption (long "final-dir" <> short 'd' <> metavar "DIR" <> value "tmp/dl")
