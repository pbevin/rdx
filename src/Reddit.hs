{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}

module Reddit (fetchPosts) where

import qualified Control.Exception
import           Control.Lens
import           Control.Monad.IO.Class (MonadIO, liftIO)
import           Data.Aeson.Lens
import           Data.Foldable
import           Data.Maybe             (isJust)
import           Network.HTTP.Client    hiding (path)
import qualified Network.Wreq           as Wreq

import           Data.Text              (Text)
import           Post

fetchPosts :: MonadIO m => String -> Int -> ((Int, Post) -> m ()) -> m (Either HttpException ())
fetchPosts name limit callback =
  loop 1 limit (PagerState (listingUrl name, Nothing))
    where
      loop k n task
        | k > n = return (Right ())
        | otherwise =  do
           result <- liftIO $ processListing task

           case result of
             Left err -> return (Left err)
             Right (posts, nextTask) -> do
               forM_ (zip [k..] posts) callback

               case nextTask of
                 Just t -> loop (k + length posts) (n - length posts) t
                 _      -> return (Right ())

newtype PagerState = PagerState (String, Maybe Text) deriving (Show, Eq)

processListing :: PagerState -> IO (Either HttpException ([Post], Maybe PagerState))
processListing (PagerState (url, after)) = do
  let opts = Wreq.defaults & Wreq.param "after" .~ toList after
  Control.Exception.try $ do
    r <- Wreq.getWith opts url
    let (posts, nextToken) = parseListing r

    if isJust nextToken && not (null posts)
       then return (posts, Just (PagerState (url, nextToken)))
       else return (posts, Nothing)

parsePost :: AsValue s => Traversal' s Post
parsePost = key "data" . key "children" . _Array . each . key "data" . _JSON

parseToken :: AsValue s => Traversal' s Text
parseToken = key "data" . key "after" . _String

parseListing :: AsValue a => Response a -> ([Post], Maybe Text)
parseListing r =
  let posts = r ^.. Wreq.responseBody . parsePost
      nextToken = r ^. Wreq.responseBody . pre parseToken
   in (posts, nextToken)

listingUrl :: String -> String
listingUrl name = "https://www.reddit.com/r/" ++ name ++ ".json"
