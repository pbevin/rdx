module Download (toTemp) where

import           Control.Exception
import           Control.Monad     (when)
import qualified Data.Maybe        as Maybe
import           Network.URI       (URI)
import qualified Network.URI       as URI
import qualified System.Directory  as Dir
import           System.FilePath
import qualified System.Process    as Proc

import           Hash              (hash, pack58)

toTemp :: URI -> IO (Either IOException FilePath)
toTemp uri = try $ do
  let dir = "tmp" </> "z"
  Dir.createDirectoryIfMissing True dir
  let path = dir </> pack58 (hash uri)

  case (hostname uri, extension uri) of
    ("www.reddit.com", _) -> fail "self post"
    ("v.redd.it", _)  -> youtubedl uri path
    ("gfycat.com", _) -> youtubedl uri path
    (_, "gifv")       -> youtubedl uri path
    ("imgur.com", _)  -> imgur uri path
    _                 -> direct uri path

  return path

youtubedl :: URI -> FilePath -> IO ()
youtubedl uri path =
  Proc.callProcess "youtube-dl" [ "-q", "-f", "mp4", "-o", path, show uri ]

imgur :: URI -> FilePath -> IO ()
imgur uri path = do
  when (take 8 (URI.uriPath uri) == "/gallery") $ fail "imgur gallery"
  curl ("https://i.imgur.com/" ++ URI.uriPath uri ++ "." ++ "jpg") path

direct :: URI -> FilePath -> IO ()
direct uri path =
  curl (show uri) path

hostname :: URI -> String
hostname uri =
  Maybe.fromMaybe "" (URI.uriRegName <$> URI.uriAuthority uri)

extension :: URI -> String
extension = reverse . takeWhile (/= '.') . takeWhile (/= '/') . reverse . URI.uriPath

curl :: String -> FilePath -> IO ()
curl url path =
  Proc.callProcess "curl" [ "-s", "-o", path, url ]
