{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module DB
  ( DB
  , runDB
  , connection
  , options
  ) where


import           Control.Exception          (bracket)
import           Control.Monad.Reader
import qualified Data.ByteString.Char8      as C
import           Database.PostgreSQL.Simple (Connection, close,
                                             connectPostgreSQL)
import           System.Environment         (getEnv)


import qualified Args

newtype DB a = DB (ReaderT (Connection, Args.Options) IO a)
  deriving (Functor, Applicative, Monad, MonadReader (Connection, Args.Options), MonadIO)

runDB :: Args.Options -> DB a -> IO a
runDB opts (DB m) = do
  connString <- C.pack <$> getEnv "DATABASE"
  bracket
    (connectPostgreSQL connString)
    close
    (\conn -> runReaderT m (conn, opts))


connection :: DB Connection
connection = fst <$> ask

options :: DB Args.Options
options = snd <$> ask
