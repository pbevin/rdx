{-# LANGUAGE OverloadedStrings #-}

module Post where

import           Data.Aeson

data Post =
  Post
    { postId        :: String
    , postTitle     :: String
    , postUrl       :: String
    , postSubreddit :: String
    , postScore     :: Int
    } deriving (Show, Eq)

instance FromJSON Post where
  parseJSON = withObject "Post" $ \v -> Post
    <$> v .: "id"
    <*> v .: "title"
    <*> v .: "url"
    <*> v .: "subreddit"
    <*> v .: "score"

instance ToJSON Post where
  toJSON (Post id_ title url subreddit score) =
    object
      [ "id" .= id_
      , "title" .= title
      , "url" .= url
      , "subreddit" .= subreddit
      , "score" .= score
      ]
