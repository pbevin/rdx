module Hash
  ( ContentHash
  , hash
  , hashFile
  , sha1
  , pack58
  ) where

import qualified Crypto.Hash.SHA1      as SHA
import qualified Data.ByteString       as B
import qualified Data.ByteString.Lazy  as L
import qualified Data.ByteString.Char8 as C
import           Data.Word             (Word8)
import           Numeric (showIntAtBase, showHex)


newtype ContentHash = H Integer
  deriving Eq

hash :: Show a => a -> ContentHash
hash = H . bsToNum . SHA.hash . C.pack . show

hashFile :: FilePath -> IO ContentHash
hashFile path = H . bsToNum . SHA.hashlazy <$> L.readFile path

bsToNum :: B.ByteString -> Integer
bsToNum = B.foldl' g 0
  where
    g :: Integer -> Word8 -> Integer
    g n w8 = 256 * n + fromIntegral w8

pack58 :: ContentHash -> String
pack58 (H n) = hashToByteString n

base58 :: Int -> Char
base58 = ("123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz" !!)

hashToByteString :: Integer -> String
hashToByteString b = take 7 . reverse $ showIntAtBase 58 base58 b ""

sha1 :: ContentHash -> String
sha1 (H n) = reverse (take 64 z)
  where
    z = reverse (showHex n "") ++ repeat '0'
