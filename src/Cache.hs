{-# LANGUAGE MultiWayIf        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module Cache
  ( PostStatus(..)
  , DownloadStatus(..)
  , CachedFile
  , findPost
  , notifyDownload
  , link
  ) where

import           Control.Exception
import           Control.Monad                        (void)
import           Control.Monad.IO.Class               (liftIO)
import qualified Data.ByteString                      as B
import qualified Data.ByteString.Char8                as C
import           Data.Text                            (Text)
import qualified Data.Text                            as T
import           Database.PostgreSQL.Simple
import           Database.PostgreSQL.Simple.FromField
import           Database.PostgreSQL.Simple.ToField
import           Post                                 (Post)
import qualified Post
import qualified System.Directory                     as Dir
import           System.FilePath
import           System.IO
import           System.IO.Error                      (isAlreadyExistsError)
import qualified System.Posix.Files                   as Posix
import           Text.RawString.QQ

import qualified Args
import           DB                                   (DB, connection, options)
import qualified Hash                                 as H

data PostStatus = NewPost | OldPost deriving (Show, Eq)

data DownloadStatus
  = Cached CachedFile
  | NotWanted
  | Remote
  deriving (Show, Eq)


data CachedFile = CachedFile ImgType String deriving (Show, Eq)


data RedditPost = RedditPost
  { subreddit :: Text
  , redditId  :: Text
  , url       :: Text
  , title     :: Text
  , maxScore  :: Int
  , minPos    :: Int
  } deriving (Show, Eq)


data ImgType
  = ImgJpeg
  | ImgGif
  | ImgPng
  | ImgMovie
  deriving (Show, Eq)

instance FromField ImgType where
  fromField _ (Just "jpg")   = return ImgJpeg
  fromField _ (Just "gif")   = return ImgGif
  fromField _ (Just "png")   = return ImgPng
  fromField _ (Just "movie") = return ImgMovie
  fromField f (Just v)       = returnError ConversionFailed f (C.unpack v)
  fromField f Nothing        = returnError UnexpectedNull f ""

instance ToField ImgType where
  toField ImgJpeg  = Escape "jpg"
  toField ImgGif   = Escape "gif"
  toField ImgPng   = Escape "png"
  toField ImgMovie = Escape "movie"

imgFileExt :: ImgType -> String
imgFileExt ImgJpeg  = "jpg"
imgFileExt ImgGif   = "gif"
imgFileExt ImgPng   = "png"
imgFileExt ImgMovie = "mp4"

findPost :: Int -> Post -> DB (Either IOException (PostStatus, DownloadStatus))
findPost rank post = do
  conn <- connection
  cacheDir <- Args.cacheDir <$> options
  liftIO $ try (findRedditPost conn cacheDir rank post)


findRedditPost :: Connection -> FilePath -> Int -> Post -> IO (PostStatus, DownloadStatus)
findRedditPost conn cacheDir rank post = do
  postStatus <- upsertRedditPost conn (newRedditPost rank post)
  downloadStatus <- checkDownload conn cacheDir (Post.postUrl post)
  return (postStatus, downloadStatus)
  -- download <- upsertDownload conn (postUrl post)
  -- if postIsNew
  --    then return (NewPost, Remote)
  --    else checkDownload conn cacheDir (Post.postUrl post)


checkDownload :: Connection -> FilePath -> String -> IO DownloadStatus
checkDownload conn cacheDir downloadUrl = do
  result <- query conn
    [r| SELECT sha1 FROM downloads WHERE url = ? LIMIT 1 |]
    (Only downloadUrl)
  case result of
    []                   -> return Remote
    Only (Just sha1) : _ -> checkImage conn cacheDir sha1
    Only Nothing : _     -> return NotWanted


checkImage :: Connection -> FilePath -> String -> IO DownloadStatus
checkImage conn cacheDir sha1 = do
  result <- query conn
    [r| SELECT imgtype, basename FROM images WHERE sha1 = ? LIMIT 1 |]
    (Only sha1)
  case result of
    []         -> return NotWanted
    (f, b) : _ -> do
      exists <- Dir.doesFileExist (cacheDir </> b)
      if exists
        then return (Cached (CachedFile f b))
        else return Remote


newRedditPost :: Int -> Post -> RedditPost
newRedditPost rank post =
  RedditPost
    { subreddit = T.pack $ Post.postSubreddit post
    , redditId = T.pack $ Post.postId post
    , url = T.pack $ Post.postUrl post
    , title = T.pack $ Post.postTitle post
    , maxScore = Post.postScore post
    , minPos = rank
    }

upsertRedditPost :: Connection -> RedditPost -> IO PostStatus
upsertRedditPost conn post = do
  isNewPost <- wasNew <$> query conn
    [r| INSERT INTO reddit_posts
      (subreddit, reddit_id, url, title, max_score, min_pos, is_new)
      VALUES (?, ?, ?, ?, ?, ?, TRUE)
      ON CONFLICT (subreddit, reddit_id) DO UPDATE
        SET max_score = GREATEST(reddit_posts.max_score, EXCLUDED.max_score),
            min_pos = LEAST(reddit_posts.min_pos, EXCLUDED.min_pos),
            is_new = FALSE
      RETURNING is_new
    |]
    (subreddit post, redditId post, url post, title post, maxScore post, minPos post)

  if isNewPost
     then return NewPost
     else return OldPost

wasNew :: [Only Bool] -> Bool
wasNew [Only x] = x
wasNew _        = False


-- DOWNLOADS

notifyDownload :: Post -> Either IOException FilePath -> DB (Maybe CachedFile)
notifyDownload post result =
  case result of
    Left err   -> notifyDownloadError post (show err) >> return Nothing
    Right path -> importFile post path


notifyDownloadError :: Post -> String -> DB ()
notifyDownloadError post err = do
  conn <- connection
  void $ liftIO $ execute conn
    [r| INSERT INTO downloads (url, error) VALUES (?, ?)|]
    (Post.postUrl post, err)

importFile :: Post -> FilePath -> DB (Maybe CachedFile)
importFile post path = do
  bytes <- liftIO $ withFile path ReadMode $ \h -> B.hGet h 8
  if | B.take 2 bytes == "\xff\xd8" -> Just <$> takeoverFile ImgJpeg post path
     | B.take 3 bytes == "GIF"      -> Just <$> takeoverFile ImgGif post path
     | B.take 4 bytes == "\x89PNG"  -> Just <$> takeoverFile ImgPng post path
     | B.drop 4 bytes == "ftyp"     -> Just <$> takeoverFile ImgMovie post path
     | otherwise -> return Nothing

takeoverFile :: ImgType -> Post -> FilePath -> DB CachedFile
takeoverFile imgType post path = do
  h <- liftIO $ H.hashFile path
  let sha1 = H.sha1 h
      basename = H.pack58 h
  conn <- connection
  void $ liftIO $ execute conn
    [r| INSERT INTO images (sha1, basename, imgtype) VALUES (?, ?, ?) ON CONFLICT DO NOTHING|]
    (sha1, basename, imgType)
  void $ liftIO $ execute conn
    [r| INSERT INTO downloads (url, sha1) VALUES (?, ?) ON CONFLICT DO NOTHING|]
    (Post.postUrl post, sha1)
  dir <- Args.cacheDir <$> options
  liftIO $ do
    Dir.createDirectoryIfMissing True dir
    Dir.renameFile path (dir </> basename)
    return (CachedFile imgType basename)


link :: CachedFile -> FilePath -> String -> DB ()
link (CachedFile imgType basename) subdir filename = do
  let ext = imgFileExt imgType
  finalDir <- Args.finalDir <$> options
  cacheDir <- Args.cacheDir <$> options
  liftIO $ do
    Dir.createDirectoryIfMissing True (finalDir </> ext </> subdir)
    let fromPath = cacheDir </> basename
        toPath = finalDir </> ext </> subdir </> filename <.> ext
    result <- try (Posix.createLink fromPath toPath)
    case result of
      Right () -> return ()
      Left e ->
        if isAlreadyExistsError e
           then Dir.removeFile fromPath
           else return ()
